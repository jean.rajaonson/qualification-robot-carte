##première partie

from random import randint


def crea_jeu ( ) :
    jeu = []
    oranges = [0 for i in range(4)]
    vertes = [i for i in range(1, 11)]
    violettes = [ i*10 for i in vertes if i%2 != 0]
    violettes = violettes * 2
    for i in vertes :
        jeu.append((1, i))
    for i in violettes :
        jeu.append((0, i))
    for i in oranges :
        jeu.append((2, i))
    return jeu

def distribue(jeu) :
    jeu1, jeu2 = [], []
    while len(jeu) != 0 :
        a = randint(0, len (jeu) - 1)
        jeu1.append(jeu[a])
        del jeu[a]
        b = randint(0, len(jeu) - 1 )
        jeu2.append(jeu[b])
        del jeu[b]
    return jeu1, jeu2 ## retourne deux tuples

def pli (carte1, carte2) :
    """ carte1 et carte2 : tuples de la forme (Couleur, Valeur) """
    """ retourne un tuple : carte ayant remporte le pli """
    """ retourne None en cas d'egalite """
    if carte1[0] > carte2[0] : # On vérifie la couleur des cartes
        return carte1
    elif carte1[0] < carte2[0] :
        return carte2
    else :
        if carte1[1] > carte2[1] : # si les couleurs sont identiques, on vérifie leur valeur
            return carte1
        elif carte1[1] < carte2[1] :
            return carte2
    return None # si les cartes sont identiques on retourne None

def jouer_carte1(main) :
    """ main : la liste non vide de cartes représentant le jeu de la petite nièce 
    la fonction retourne la carte jouée """
    liste1 = [i[1] for i in main if i[0] == 1]     
    liste2 = [i[1] for i in main if i[0] == 0]
    
    liste_f = [[i for i in main if i[0] == 2], 
              [i for i in main if (i[0] == 0 and i[1] == min(liste2))],
              [i for i in main if (i[0] == 1 and i[1] == max(liste1))]]
    
    for partie in liste_f:
        if partie != []:
            for carte in partie:
                return carte

def jouer_carte2(main) :
    """ main est une liste de cartes que peut jouer le programme
    la fonction retourne la carte jouée"""
    ## stratégie du robot : 
    ## 1 : il va commencer par jouer ses cartes vertes les plus fortes jusqu'aux plus faibles
    ## 2 : il va continuer par jouer ses cartes violettes les plus faibles aux plus fortes
    ## 3 : et fini par jouer toutes ses cartes orange
    liste1 = [i[1] for i in main if i[0] == 1] ## met dans la liste 1 dans le cas où la couleur est vertes 
    liste2 = [i[1] for i in main if i[0] == 0] ## met dans la liste 2 dans le cas où la couleur est violettes
    for i in range(len(main)) :
        if main[i] == 2 : ## cette condition est un leurre, elle ne renvoie pas une carte orange
            ## la raison est que, une carte est défini par un couple de valeur, donc si on prend juste la ième carte, on
            ## ne peut pas savoir sa couleur
            carte = main[i]
            return carte 
    for i in range(len(main)) :
        if main[i][0] == 1 and main[i][1] == max(liste1) : ## si la couleur de la carte est verte, et si le nombre de la carte est le plus grand de la liste 1
            carte = main[i]
            return carte ## joue une carte verte
    for i in range(len(main)) :
        if main[i][0] == 0 and main[i][1] == min(liste2) : ## si la couleur est violette, et si le nombre est le plus petit de la liste 2
            carte = main[i]
            return carte ## joue une carte violette
    carte = main[0] ## renvoie une carte orange en réponse au leurre, car il ne reste que cela dans sa main
    return carte

def partie() :
    joueur1, joueur2 = distribue(crea_jeu())
    score1, score2 = 0, 0
    while len(joueur1) != 0 and len(joueur2) != 0 :
        carte_j1 = jouer_carte1(joueur1)
        joueur1.remove(carte_j1)
        carte_j2 = jouer_carte2(joueur2)
        joueur2.remove(carte_j2)
        gagnant = pli(carte_j1, carte_j2)
        if gagnant == carte_j1 :
            score1 = score1 + carte_j1[1] + carte_j2[1]
        elif gagnant == carte_j2 :
            score2 = score2 + carte_j1[1] + carte_j2[1] 
    if score1 > score2 :
        return 1
    elif score1 < score2 :
        return 2
    else :
        return 0

def parties(n) :
    g1, g2 = 0,0
    for i in range (n) :
        resultat = partie()
        if resultat == 1 :
            g1 += 1
        elif resultat == 2 :
            g2 += 1
    return g1/n*100, g2/n*100
